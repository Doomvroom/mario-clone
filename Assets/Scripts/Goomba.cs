﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goomba : MonoBehaviour {

    public float walkSpeed = 5.0f;
    private bool walkLeft = true;
    private Animator animator;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (walkLeft)
        {
            transform.Translate(Vector3.left * walkSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * walkSpeed * Time.deltaTime);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var mario = collision.gameObject.GetComponent<Mario>();

        if (mario)
        {
            animator.Play("Goomba_Death");
        }
        else
        {
            walkLeft = !walkLeft;
        }

    }

    public void Die()
    {
        Destroy(this.gameObject);
    }

}
