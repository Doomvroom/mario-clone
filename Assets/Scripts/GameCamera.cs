﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{


    public float smoothTime = 0.1F;          // camera dampener
    public bool cameraFollowX = true;
    public bool cameraFollowY = true;
    public bool cameraFollowHeight = false;
    public float cameraHeight = 2.5f;        //adjustable height of camera

    public GameObject cameraTarget;         // what we are following
    public GameObject player;               //player object moving
    public Vector2 velocity;                // speed of the camera movement

    void Update()
    {
        if (cameraFollowX)
        {
            var x =  Mathf.SmoothDamp(transform.position.x, cameraTarget.transform.position.x, ref velocity.x, smoothTime);
            Vector3 vector = new Vector3(x, transform.position.y, transform.position.z);
            transform.position = vector;
        }

        if (cameraFollowY)
        {

        }

        if (!cameraFollowY && cameraFollowHeight)
        {

        }
    }
}
