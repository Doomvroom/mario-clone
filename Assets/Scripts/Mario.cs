﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mario : MonoBehaviour {

    public float speed = 5.0f;
    public float jumpSpeed = 400f;

    private Animator marioAnimator;
    private Boots boots;
    private Rigidbody2D rigidBody;
    private float scaleX = 1.0f;
    private float scaleY = 1.0f;

    void Start()
    {
        marioAnimator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
        boots = FindObjectOfType<Boots>();

    }

    void FixedUpdate()
    {
        RotationHack();
        float mSpeed = Input.GetAxis("Horizontal");
        marioAnimator.SetFloat("Speed", Mathf.Abs(mSpeed));

        if (Input.GetKey(KeyCode.Space))
        {
            if (boots.CanJump())
            {
                rigidBody.AddForce(Vector2.up * jumpSpeed);
                boots.Jump();
            }
        }
       

        if (mSpeed > 0){
            transform.localScale = new Vector2(scaleX, scaleY);
        }
		else if (mSpeed < 0){
            transform.localScale = new Vector2(-scaleX, scaleY);
        }

       rigidBody.velocity = new Vector2(mSpeed * speed, rigidBody.velocity.y);
    }

    private void RotationHack()
    {
        Quaternion theRotation = transform.localRotation;
        theRotation.z = 0;
        transform.localRotation = theRotation;
    }
}
