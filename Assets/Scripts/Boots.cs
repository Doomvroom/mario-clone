﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boots : MonoBehaviour
{
    private bool canJump = false;
    private Animator marioAnimator;

    void Start()
    {
        marioAnimator = transform.parent.GetComponent<Animator>();
    }


    public bool CanJump()
    {
        return canJump;
    }

    public void Jump()
    {
        UpdateAnimation(true);
    }

    private void UpdateAnimation(bool jumping)
    {
        marioAnimator.SetBool("canJump", jumping);
        canJump = !jumping;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        UpdateAnimation(false);
    }
}
